# Simple PostgreSQL setup

Personal playbook used with Linode, CentOS7 and S3 for backups.

# Vagrant setup

## Ansible dependencies

    $ ansible-galaxy install -r requirements.yml

## Vagrant up

    $ vagrant up
    $ vagrant ssh-config > ~/.ssh/vagrant.conf
    $ ansible-playbook -i inventory/default/hosts site.yml

That should be enough to connect from your host with psql using self-signed TLS.

## Reset server

    $ ansible-playbook -i inventory/default/hosts site.yml -e 'reset_postgresql=True'

# TODO

* CentOS 8 support when the time comes.
